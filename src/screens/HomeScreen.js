import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity, Image, ActivityIndicator } from 'react-native';
import SearchBar from '../components/SearchBar';
import tmdb from '../api/tmdb';
import { Picker } from '@react-native-picker/picker';

const HomeScreen = ({ navigation }) => {
  const [text, setText] = useState('');
  const [results, setResults] = useState([]);
  const [loading, setLoading] = useState(false);
  const [category, setCategory] = useState('movie');

  useEffect(() => {  
    searchTmdb('Fight Club');
  }, []);

  const searchTmdb = async (query) => {
    setLoading(true);
    try {
      const response = await tmdb.get(`/search/${category}`, {
        params: {
          query,
          include_adult: false,
        },
      });
      setResults(response.data.results);
    } catch (err) {
      console.log(err);
    } finally {
      setLoading(false);
    }
  };

  const renderResultItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => navigation.navigate('Details', { id: item.id, category })}
      >
        <View style={styles.imageContainer}>
          {item.poster_path ? (
            <Image
              source={{ uri: `https://image.tmdb.org/t/p/w500/${item.poster_path}` }}
              style={styles.image}
            />
          ) : (
            <Text>N/A</Text>
          )}
        </View>
        <Text>{item.original_title}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.pickerContainer}>
        <Picker
          selectedValue={category}
          onValueChange={(itemValue) => setCategory(itemValue)}
        >
          <Picker.Item label="Filmes" value="movie" />
          <Picker.Item label="Séries" value="tv" />
          <Picker.Item label="Personalidades" value="person" />
        </Picker>
      </View>
      <SearchBar
        onChangeText={(t) => setText(t)}
        onEndEditing={(t) => searchTmdb(t)}
        value={text}
      />
      {loading ? (
        <ActivityIndicator size="small" color="gray" />
      ) : (
        <FlatList
          data={results}
          keyExtractor={(item) => `${item.id}-${category}`}
          renderItem={renderResultItem}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  pickerContainer: {
    backgroundColor: 'lightgray',
    paddingHorizontal: 10,
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'lightgray',
  },
  imageContainer: {
    marginRight: 10,
  },
  image: {
    width: 50,
    height: 75,
  },
});

export default HomeScreen;
